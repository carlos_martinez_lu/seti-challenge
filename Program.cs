﻿/*
    _  _    _____ ______ _______ _____ _____                             _    _____ _           _ _                       
  _| || |_ / ____|  ____|__   __|_   _|  __ \                           | |  / ____| |         | | |                      
 |_  __  _| (___ | |__     | |    | | | |  | | ___  ___ _ __ _   _ _ __ | |_| |    | |__   __ _| | | ___ _ __   __ _  ___ 
  _| || |_ \___ \|  __|    | |    | | | |  | |/ _ \/ __| '__| | | | '_ \| __| |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \
 |_  __  _|____) | |____   | |   _| |_| |__| |  __/ (__| |  | |_| | |_) | |_| |____| | | | (_| | | |  __/ | | | (_| |  __/
   |_||_| |_____/|______|  |_|  |_____|_____/ \___|\___|_|   \__, | .__/ \__|\_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|
                                                              __/ | |                                           __/ |     
                                                             |___/|_|                                          |___/      


What is the typical body height of our interstellar counterparts?
    Message has been send on a 452.12919MHz fraquancy, with a wavelenght of 0.6635271657642808 meters (http://www.csgnetwork.com/freqwavelengthcalc.html)
    comparing 4th and 5th images frames we can compute they have an height of about 1.39 meters

What is their typical lifetime?

What is the scale of the devices they used to submit their message?

Since when have they been communicating interstellar?
    At least 50 years (The source is a star about 50 light years from Earth)
What kind of object do they live on?

How old is their stellar system?

*/
namespace SETI.Reader
{
    using System;
    using System.Drawing;
    using System.IO;

    static class Program
    {
        /// <summary>
        /// Following constant have the original text stream from SETI challenge
        /// </summary>
        public const string filename = @"C:\Users\cama\Desktop\SETI_message.txt";

        /// <summary>
        /// Computes the image line width based on first continous line on stream
        /// </summary>
        /// <param name="filename">message stream file</param>
        /// <returns>line width</returns>
        private static int ComputeLineWidth(string filename)
        {
            StreamReader fileStream = File.OpenText(filename);
            char[] buffer = new char[1024];
            int bytes = -1;
            Int64 totalBytes = 0;
            int lineWidth = 0;
            bool lineWidthSolved = false;

            while (bytes != 0 && !lineWidthSolved)
            {
                bytes = fileStream.ReadBlock(buffer, 0, 1024);
                totalBytes += bytes;

                if (!lineWidthSolved)
                {
                    for (int x = 0; x < bytes; x++)
                    {
                        if (buffer[x] == '0')
                        {
                            lineWidthSolved = true;
                        }
                        else
                        {
                            lineWidth++;
                        }
                    }
                }
            }

            fileStream.Close();
            fileStream.Dispose();
            fileStream = null;

            return lineWidth - 1;
        }

        /// <summary>
        /// Computes numbers of lines based on filesize and line width
        /// </summary>
        /// <param name="filename">message stream file</param>
        /// <param name="lineWidth">computed line width</param>
        /// <returns>number of lines in file</returns>
        private static long ComputeLineCount(string filename, int lineWidth)
        {
            FileInfo f = new FileInfo(filename);
            return f.Length / lineWidth;
        }

        /// <summary>
        /// Converts the text stream message provided for #SETIDecryptChallenge into a PNG file.   
        /// The firsts 1's in stream are used to determine a line width on image.  
        /// </summary>
        /// <param name="filename"></param>
        public static void CreateImage(string filename)
        {
            /// Compute image size
            int lineWidth = ComputeLineWidth(filename);
            long lineCount = ComputeLineCount(filename, lineWidth);
            System.Diagnostics.Debug.WriteLine(string.Format("Image size: {0}x{1}", lineWidth, lineCount));

            /// Create new bitmap
            Bitmap bitmap = new Bitmap(lineWidth, (int)lineCount);
            Graphics graphics = Graphics.FromImage(bitmap);

            /// Read text stream
            StreamReader fileStream = File.OpenText(filename);
            char[] buffer = new char[lineWidth];
            int bytes = -1;
            int currentLine = 0;

            while (bytes != 0)
            {
                bytes = fileStream.ReadBlock(buffer, 0, lineWidth);
                for (int i = 0; i < bytes; i++)
                {
                    /// Set pixels for ones
                    if (buffer[i] == '1')
                    {
                        graphics.FillRectangle(SystemBrushes.ControlDark, i, currentLine, 1, 1);
                    }
                }
                currentLine++;
            }


            /// CLose text stream file
            fileStream.Close();
            fileStream.Dispose();
            fileStream = null;

            /// Write resulting image
            string result = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename) + ".png");
            bitmap.Save(result, System.Drawing.Imaging.ImageFormat.Png);

            /// dispose bitmap
            graphics.Dispose();
            graphics = null;
            bitmap.Dispose();
            bitmap = null;
        }

        /// <summary>
        /// Reverse a string
        /// </summary>
        /// <param name="s">string to reverse</param>
        /// <returns>reversed string</returns>
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }


        /// <summary>
        /// Analyse frame to reveal its binary contents into Output window
        /// </summary>
        /// <param name="frameIndex">number of image frame</param>
        /// <param name="filename">text stream filename</param>
        /// <param name="fullCheck">If true, extract all the binary contents, if false, just check first n lines</param>
        public static void AnalyzeFrame(int frameIndex, string filename, bool fullCheck)
        {
            const int width = 359;
            const int height = 757;
            StreamReader fileStream = File.OpenText(filename);
            char[] buffer = new char[width * height];
            int bytes = 0;

            // read second frame
            for (int i = 0; i < frameIndex; i++)
            {
                bytes = fileStream.ReadBlock(buffer, 0, width * height);
            }

            int charCount = 0;
            int lineCount = 0;
            string line = string.Empty;

            for(int i=0; i<bytes; i++)
            {
                line += buffer[i];
                charCount++;
                if(charCount == width)
                {
                    lineCount++;
                    line = Reverse(line);
                    
                    

                    if (fullCheck)
                    {
                        if (line.LastIndexOf('1') != -1)
                        {
                            line = line.Substring(line.IndexOf('1'));
                        }
                        else
                        {
                            line = "0";
                        }
                        System.Diagnostics.Debug.WriteLine(string.Format("{0} : {1}", line, Convert.ToInt64(line, 2)));
                    }
                    else {
                        System.Diagnostics.Debug.WriteLine(string.Format("{0}", line));
                    }

                    line = string.Empty;
                    if (lineCount == 2 && !fullCheck)
                        break;
                    else
                        charCount = 0;
                }
            }

            /// CLose text stream file
            fileStream.Close();
            fileStream.Dispose();
            fileStream = null;
        }

        [STAThread]
        public static void Main()
        {
            /// Create image file from text stream
            CreateImage(filename);

            /// Manually analysing the resulting image, we know there are 7 blocks or images of 359*757 pixels
            AnalyzeFrame(2, filename, true);

            /// Analysing second frame, we got numbering from 0 to 756 in binary
            AnalyzeFrame(2, filename, true);

            /// Analysing third frame, we got prime numbers from 2 to 5749 in binary
            AnalyzeFrame(3, filename, true);


            /*
                What does the 2-line header in each frame represent???
            */
            System.Diagnostics.Debug.WriteLine("Wave header");
            AnalyzeFrame(4, filename, false);
            System.Diagnostics.Debug.WriteLine("Alien header");
            AnalyzeFrame(5, filename, false);
            System.Diagnostics.Debug.WriteLine("Radiotelescopes header");
            AnalyzeFrame(6, filename, false);
            System.Diagnostics.Debug.WriteLine("Star / Planet / Moon header");
            AnalyzeFrame(7, filename, false);


        }
    }
}
